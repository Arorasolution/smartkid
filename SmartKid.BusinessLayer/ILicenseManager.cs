using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;

namespace SmartKid.BusinessLayer
{
    public interface ILicenseManager
    {
        Task<string> RequestFeaturePurchaseAsync(string productId);
        Task<ProductListing> GetProductListingAsync(string productId);
        ProductLicense GetProductLicense(string productId);
        Task<bool> CheckProcuctLicenseAsync(string productId);
    }

    public class LicenseManager : ILicenseManager
    {
        public async Task<bool> CheckProcuctLicenseAsync(string productId)
        {
            if (IsActive())
                return true;

            var productInfo =  await GetProductListingAsync(productId);

            if (productInfo == null)
                return true;

            var featureLicense = GetProductLicense(productId);

            if (featureLicense == null)
                return false;

            return featureLicense.IsActive;
        }

        public bool IsActive()
        {
#if DEBUG
            var license = CurrentAppSimulator.LicenseInformation;
#endif
#if RELEASE
            var license = CurrentApp.LicenseInformation;
#endif

            return license.IsActive && !license.IsTrial;
        }

        public ProductLicense GetProductLicense(string productId)
        {
#if DEBUG
            var license = CurrentAppSimulator.LicenseInformation;
#endif
#if RELEASE
            var license = CurrentApp.LicenseInformation;
#endif

            if (license == null)
                return null;

            if (license.ProductLicenses.ContainsKey(productId))
            {
                return license.ProductLicenses[productId];
            }

            return null;
        }

        public async Task<ProductListing> GetProductListingAsync(string productId)
        {
            try
            {
#if DEBUG
                var listing = await CurrentAppSimulator.LoadListingInformationAsync();
#endif
#if RELEASE
                var listing = await CurrentApp.LoadListingInformationAsync();
#endif
                if (listing == null)
                    return null;

                if (listing.ProductListings.ContainsKey(productId))
                {
                    return listing.ProductListings[productId];
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async  Task<string> RequestFeaturePurchaseAsync(string featureId)
        {
#if DEBUG
            var receipt = await CurrentAppSimulator.RequestProductPurchaseAsync(featureId, true);
#endif
#if RELEASE
            var receipt = await CurrentApp.RequestProductPurchaseAsync(featureId, true);
#endif
            return receipt;
        }

    }
}
