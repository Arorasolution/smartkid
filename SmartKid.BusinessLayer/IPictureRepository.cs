﻿using SmartKid.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartKid.BusinessLayer
{
    public interface IPictureRepository
    {
        Task<IEnumerable<Category>> GetAllCategories();
        Task<IEnumerable<Picture>> Search(string terms);
    }

    public interface IPictureRepositoryFactory
    {
        IPictureRepository CreatePicturerepository();
    }

    public class PictureRepositoryFactory : IPictureRepositoryFactory
    {
        IPictureRepository _RepositoryInstance;
        public IPictureRepository CreatePicturerepository()
        {
            if (_RepositoryInstance == null)
                _RepositoryInstance = new FolderStructurePictureRepository();

            return _RepositoryInstance;
        }
    }


}
