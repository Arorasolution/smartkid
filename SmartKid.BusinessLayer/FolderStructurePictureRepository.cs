﻿using SmartKid.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;
using System.IO;

namespace SmartKid.BusinessLayer
{
    public class FolderStructurePictureRepository : IPictureRepository
    {

        private readonly ILicenseManager _licenseManager;
        private static List<Category> _result;


        public FolderStructurePictureRepository() : this(new LicenseManager())
        {

        }

        public FolderStructurePictureRepository(ILicenseManager _LicenseManager)
        {
            _licenseManager = _LicenseManager;
        }


        public async Task<IEnumerable<Category>> GetAllCategories()
        {
            if (_result != null)
                return _result;

            var imagesFolder = await Package.Current.InstalledLocation.GetFolderAsync("image");
            _result = new List<Category>();

            IReadOnlyList<StorageFolder> categoryList = await imagesFolder.GetFoldersAsync();
            foreach (StorageFolder category in categoryList)
            {
                var cModel = new Category()
                {
                    Name = category.Name
                };
                _result.Add(cModel);

                var galleryFolder = await imagesFolder.GetFolderAsync(category.Name);
                var categoryfile = await category.GetFilesAsync();
                string categoryIdText = await Windows.Storage.FileIO.ReadTextAsync(categoryfile.First(x => x.Name.IndexOf("txt") >= 0));

                cModel.Id = new Guid(categoryIdText);
                IReadOnlyList<StorageFolder> galleryList = await galleryFolder.GetFoldersAsync();

                var gallery = new List<Gallery>();
                foreach (StorageFolder galleryFD in galleryList)
                {
                    var gModel = new Gallery(new Uri("file://" + galleryFD.Path + "/Thumbnail/thumbnail.png"))
                    {
                        Category = _result[0].Name,
                        Title = galleryFD.Name,
                    };

                    gallery.Add(gModel);
                    var picturesFolder = await galleryFolder.GetFolderAsync(galleryFD.Name);
                    IReadOnlyList<StorageFile> pictureFileList = await picturesFolder.GetFilesAsync();
                    string galleryIdtext = await Windows.Storage.FileIO.ReadTextAsync(pictureFileList.First(x => x.Name.IndexOf("txt") >= 0));
                    gModel.Id = new Guid(galleryIdtext);
                    gModel.IsLocked = await CheckProductLocked(gModel.Id);

                    //var pictures = new Dictionary<string, List<Picture>>();
                    var picture = new List<Picture>();
                    foreach (StorageFile pictureFile in pictureFileList.Where(x=>x.Name.IndexOf("txt") < 0))
                    {
                        string[] str = Path.GetFileNameWithoutExtension(pictureFile.Name).Split('_');
                   
                        
                        picture.Add(new Picture(new Uri("file://" + pictureFile.Path), gModel)
                        {
                            Id = Guid.NewGuid(),
                            Name = str[1],
                            Gallery = galleryFD.Name,
                            Order = int.Parse(str[0]),
                            Sound = new Uri("ms-appx:///Sound/" + galleryFD.Name + "/" + str[1] + ".mp3")
                        });
                    }
                    gModel.Pictures = picture.OrderBy(x=>x.Order).ToList();
                }
                cModel.Galleries = gallery;
            }
            return _result;
        }

        private async Task<bool> CheckProductLocked(Guid guid)
        {
           return  !await _licenseManager.CheckProcuctLicenseAsync(guid.ToString());
        }

        public async Task<IEnumerable<Picture>> Search(string terms)
        {
            terms = terms.ToLower().Trim();

            IEnumerable<Category> categories = await GetAllCategories();
            IEnumerable<Gallery> gal = categories.ToList()[0].Galleries;

            var result = new List<Picture>();

            try
            {
                foreach (var g in gal)
                {
                    result.AddRange(g.Pictures.Where(p => p.Name.ToLower().Trim().Contains(terms)));
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
    }
}

