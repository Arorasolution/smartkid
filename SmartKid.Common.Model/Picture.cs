﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace SmartKid.Common.Model
{
    public class Picture : BaseModel
    {
        public Picture(Uri pathUrl, Gallery parentContianer)
        {
            _Image = new BitmapImage(pathUrl);
            ParentContianer = parentContianer;
        }

        #region Fields
        private Guid _Id;
        private string _Name;
        private string _Description;
        private string _Gallery;
        private Uri _Sound;
        private int _Order;
        #endregion

        private ImageSource _Image = null;

        public Guid Id
        {
            get
            {
                return this._Id;
            }

            set
            {
                if (this._Id != value)
                {
                    this._Id = value;
                    this.OnPropertyChanged("Id");
                }
            }
        }

        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        public string Description
        {
            get
            {
                return this._Description;
            }

            set
            {
                if (this._Description != value)
                {
                    this._Description = value;
                    this.OnPropertyChanged("Description");
                }
            }
        }

        public string Gallery
        {
            get
            {
                return this._Gallery;
            }

            set
            {
                if (this._Gallery != value)
                {
                    this._Gallery = value;
                    this.OnPropertyChanged("Gallery");
                }
            }
        }

        public ImageSource Image
        {
            get
            {
                return this._Image;
            }

            set
            {
                if (this._Image != value)
                {
                    this._Image = value;
                    this.OnPropertyChanged("Image");
                }
            }
        }

        public Uri Sound
        {
            get
            {
                return this._Sound;
            }
            set
            {
                if (this._Sound != value)
                {
                    this._Sound = value;
                    OnPropertyChanged("Sound");
                }
            }
        }

        public Gallery ParentContianer { get; set; }


        public int Order
        {
            get
            {
                return this._Order;
            }

            set
            {
                if (this._Order != value)
                {
                    this._Order = value;
                    this.OnPropertyChanged("Order");
                }
            }
        }
    }
}
