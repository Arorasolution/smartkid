﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace SmartKid.Common.Model
{
    public class Gallery : BaseModel
    {
        public Gallery()
        {

        }
        public Gallery(Uri pathUrl)
        {
            _Image = new BitmapImage(pathUrl);
        }

        #region Fields
        private string _Title;
        private string _Subtitle;
        private ImageSource _Image;
        private string _Category;
        private int _index;
        private bool _IsLocked;
        #endregion

        #region Properties
        public Guid Id { get; set; }

        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                if (this._Title != value)
                {
                    this._Title = value;
                    this.OnPropertyChanged("Title");
                }
            }
        }

        public string Subtitle
        {
            get
            {
                return this._Subtitle;
            }

            set
            {
                if (this._Subtitle != value)
                {
                    this._Subtitle = value;
                    this.OnPropertyChanged("Subtitle");
                }
            }
        }

        public ImageSource Image
        {
            get
            {
                return this._Image;
            }

            set
            {
                if (this._Image != value)
                {
                    this._Image = value;
                    this.OnPropertyChanged("Image");
                }
            }
        }

        public string Category
        {
            get
            {
                return this._Category;
            }

            set
            {
                if (this._Category != value)
                {
                    this._Category = value;
                    this.OnPropertyChanged("Category");
                }
            }
        }

        public int index
        {
            get
            {
                return this._index;
            }
            set
            {
                if (this._index != value)
                {
                    this._index = value;
                    this.OnPropertyChanged("index");
                }
            }
        }


        public IEnumerable<Picture> Pictures { get; set; }

        public bool IsLocked
        {
            get
            {
                return this._IsLocked;
            }
            set
            {
                if (this._IsLocked != value)
                {
                    this._IsLocked = value;
                    this.OnPropertyChanged("IsLocked");
                }
            }
        }
        #endregion

    }
}
