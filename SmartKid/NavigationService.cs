﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace SmartKid
{
    class NavigationService : INavigationService
    {

        private static NavigationService _Instance;

        public static INavigationService Current
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new NavigationService();
                }

                return _Instance;
            }
        }

        public static Frame Frame { get; set; }

        public static string ViewNameSpace { get; set; }


        public void GoHome()
        {
            if (Frame != null)
            {
                while (Frame.CanGoBack)
                {
                    Frame.GoBack();
                }
            }
        }


        public void GoForward()
        {
            if (Frame != null && Frame.CanGoForward)
            {
                Frame.GoForward();
            }
        }

        public void GoBack()
        {
            if (Frame != null && Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }


        public void Navigate(string view, object param)
        {
            if (Frame != null)
            {
                Type myClassType = Type.GetType(string.Format("{0}.{1}", ViewNameSpace, view));
                Frame.Navigate(myClassType, param);
            }
        }

        //public bool CanGoback()
        //{
        //    throw new NotImplementedException();
        //}

        //public bool CanGoForward()
        //{
        //    throw new NotImplementedException();
        //}

        public void Navigate(string view)
        {
            this.Navigate(view, null);
        }


        public bool CanGoback
        {
            get
            {
                if (Frame != null)
                {
                    return Frame.CanGoBack;
                }

                return false;
            }
        }

        public bool CanGoForward
        {
            get
            {
                if (Frame != null)
                {
                    return Frame.CanGoForward;
                }

                return false;
            }
        }
    }
}
