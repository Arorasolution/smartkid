﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace SmartKid.Utility
{
    public interface IAudioPlayer
    {
        MediaElement MediaElementPlayer { get; set; }
        void Play(Uri file);
    }

    public class AudioPlayer : IAudioPlayer
    {
        public MediaElement MediaElementPlayer { get; set; }
        public List<Uri> Files { get; set; }

        private int _Sequence = 0;

        public int Sequence
        {
            get
            {
                return this._Sequence;
            }
        }

        public void Play(Uri file)
        {
            MediaElementPlayer.Source = file;
            MediaElementPlayer.Play();
        }
    }
}
