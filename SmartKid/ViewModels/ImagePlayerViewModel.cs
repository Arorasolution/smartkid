﻿using GalaSoft.MvvmLight.Command;
using SmartKid.BusinessLayer;
using SmartKid.Common.Model;
using SmartKid.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Media.PlayTo;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace SmartKid.ViewModels
{
    public class ImagePlayerViewModel : ViewModelBase
    {
        #region Feild
        const string STATE_CURRENTINDEX = "currentIndex";
        IPictureRepositoryFactory _pictureRepositoryFactory;
        IAudioPlayer _audioPlayer;
        DispatcherTimer _playlistTimer = null;
        #endregion

        #region Constrcutor
        public ImagePlayerViewModel()
            : this(new PictureRepositoryFactory(), new AudioPlayer())
        {

        }

        public ImagePlayerViewModel(IPictureRepositoryFactory pictureRepositoryFactory, IAudioPlayer iAudioPlayer)
        {
            _pictureRepositoryFactory = pictureRepositoryFactory;
            _audioPlayer = iAudioPlayer;
        }

        #endregion

        #region State
        public override async void LoadState(object navigationParameter, Dictionary<string, object> vmState)
        {
            _audioPlayer.MediaElementPlayer = this.Player;

            _playlistTimer = new DispatcherTimer();
            _playlistTimer.Interval = new TimeSpan(0, 0, 5);
            _playlistTimer.Tick += PlaylistTimerTick;

            var galleryID = new Guid(navigationParameter.ToString());
            IPictureRepository _GelleryManager = _pictureRepositoryFactory.CreatePicturerepository();

            IEnumerable<Category> categories = await _GelleryManager.GetAllCategories();
            Gallery gallery = categories.First(x => x.Galleries.Any(y => y.Id == galleryID)).Galleries.First(x => x.Id == galleryID);

            if (gallery != null)
            {
                foreach (var item in gallery.Pictures)
                {
                    Pictures.Add(item);
                }
                Pictures = new ObservableCollection<Picture>(gallery.Pictures);

                if (vmState != null && vmState.ContainsKey(STATE_CURRENTINDEX))
                {
                    CurrentPicture = Pictures[(int)vmState[STATE_CURRENTINDEX]];
                }
                else
                {
                    CurrentPicture = Pictures[gallery.index];
                }
            }
        }

        public override void SaveState(Dictionary<string, object> vmState)
        {
            vmState[STATE_CURRENTINDEX] = CurrentDisplayImageIndex;
        }
        #endregion

        #region Method
        int count;
        void PlaylistTimerTick(object sender, object e)
        {
            count = CurrentDisplayImageIndex;
            if (Pictures != null)
            {
                if (count == Pictures.Count - 1)
                    count = -1;
                if (count < Pictures.Count)
                {
                    count++;
                    ChangeImage();
                }

            }
        }

        private void ChangeImage()
        {
            CurrentPicture = Pictures[count];
            //_audioPlayer.Play(Pictures[count].Sound);
        }
        #endregion

        #region Command
        ICommand _PlaySoundCommand;
        public ICommand PlaySoundCommand
        {
            get
            {
                if (_PlaySoundCommand == null)
                    _PlaySoundCommand = new RelayCommand<Picture>(PlaySelectedPicture);

                return _PlaySoundCommand;
            }
        }

        private void PlaySelectedPicture(Picture picture)
        {
            if (this.IsMute)
                return;

            _audioPlayer.Play(picture.Sound);
        }



        ICommand _PlaySlideShowCommand;
        public ICommand PlaySlideShowCommand
        {
            get
            {
                if (_PlaySlideShowCommand == null)
                    _PlaySlideShowCommand = new RelayCommand(PlayNextPicture);

                return _PlaySlideShowCommand;
            }


        }

        private void PlayNextPicture()
        {
            if (Pictures != null)
            {
                IsPlayingSlideShow = true;

                _playlistTimer.Start();
               // _audioPlayer.Play(Pictures[CurrentDisplayImageIndex].Sound);
                PlaySelectedPicture(Pictures[CurrentDisplayImageIndex]);

            }

            Appbar.IsOpen = false;
        }

        ICommand _StopSlideShowCommand;
        public ICommand StopSlideShowCommand
        {
            get
            {
                if (_StopSlideShowCommand == null)
                    _StopSlideShowCommand = new RelayCommand(StopPicture);

                return _StopSlideShowCommand;
            }


        }

        private void StopPicture()
        {
            IsPlayingSlideShow = false;

            if (Pictures != null)
                _playlistTimer.Stop();

            Appbar.IsOpen = false;
        }

        ICommand _PictureChangeCommand;
        public ICommand PictureChangeCommand
        {
            get
            {
                if (_PictureChangeCommand == null)
                    _PictureChangeCommand = new RelayCommand(PictureChange);

                return _PictureChangeCommand;
            }
        }

        private void PictureChange()
        {
           // _audioPlayer.Play(Pictures[CurrentDisplayImageIndex].Sound);
            PlaySelectedPicture(Pictures[CurrentDisplayImageIndex]);
        }


        ICommand _MuteCommand;
        public ICommand MuteCommand
        {
            get
            {
                if (_MuteCommand == null)
                    _MuteCommand = new RelayCommand(Mute);

                return _MuteCommand;
            }
        }

        private void Mute()
        {
            this.IsMute = true;
        }



        ICommand _VolumeCommand;
        public ICommand VolumeCommand
        {
            get
            {
                if (_VolumeCommand == null)
                    _VolumeCommand = new RelayCommand(Volume);

                return _VolumeCommand;
            }
        }

        private void Volume()
        {
            this.IsMute = false;
        }
        #endregion

        #region Property
        ObservableCollection<Picture> _Pictures;
        public ObservableCollection<Picture> Pictures
        {
            get
            {
                if (_Pictures == null)
                    _Pictures = new ObservableCollection<Picture>();

                return _Pictures;
            }
            set
            {
                SetProperty(ref _Pictures, value);
            }
        }

        public int CurrentDisplayImageIndex
        {
            get
            {
                return Pictures.IndexOf(CurrentPicture);
            }
        }

        private Picture _CurrentPicture;
        public Picture CurrentPicture
        {
            get { return _CurrentPicture; }
            set
            {
                SetProperty(ref _CurrentPicture, value);
                //_audioPlayer.Play(_CurrentPicture.Sound);
                PlaySelectedPicture(_CurrentPicture);

            }
        }

        //private bool _IsAppbarOpen;
        //public bool IsAppbarOpen
        //{
        //    get { return _IsAppbarOpen; }
        //    set
        //    {
        //        SetProperty(ref _IsAppbarOpen, value);
        //    }
        //}


        public MediaElement Player { get; set; }

        bool _IsPlayingSlideShow;
        public bool IsPlayingSlideShow
        {
            get
            {
                return _IsPlayingSlideShow;
            }
            set
            {
                SetProperty(ref _IsPlayingSlideShow, value);
                this.OnPropertyChanged("IsAblePlaySlideShow");
                this.OnPropertyChanged("IsAbleStopSlideShow");

            }
        }

        public bool IsAblePlaySlideShow
        {
            get
            {
                return !IsPlayingSlideShow;
            }
        }

        public bool IsAbleStopSlideShow
        {
            get
            {
                return IsPlayingSlideShow;
            }
        }


        bool _IsMute;
        public bool IsMute
        {
            get
            {
                return _IsMute;
            }
            set
            {
                SetProperty(ref _IsMute, value);
                this.OnPropertyChanged("IsAbleToMute");
                this.OnPropertyChanged("IsAbleToVolumne");

            }
        }

        public bool IsAbleToMute
        {
            get
            {
                return !IsMute;
            }
        }

        public bool IsAbleToVolumne
        {
            get
            {
                return IsMute;
            }
        }
        #endregion

        public AppBar Appbar { get; set; }
    }
}
