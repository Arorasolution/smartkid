﻿using SmartKid.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartKid.ViewModels
{
    public abstract class ViewModelBase : BindableBase
    {
        public virtual void LoadState(Object navigationParameter, Dictionary<String, Object> vmState)
        {
        }

        public virtual void SaveState(Dictionary<String, Object> vmState)
        {
        }

        public INavigationService NavigationService
        {
            get;
            set;
        }

        public virtual bool CanGoback
        {
            get
            {
                if (NavigationService != null)
                    return NavigationService.CanGoback;

                return false;
            }
        }

        public virtual bool CanGoForward
        {
            get
            {
                if (NavigationService != null)
                    return NavigationService.CanGoForward;

                return false;
            }
        }
    }
}
