﻿using GalaSoft.MvvmLight.Command;
using SmartKid.BusinessLayer;
using SmartKid.Common.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmartKid.ViewModels
{
    public class SearchResultsPageViewModel : ViewModelBase
    {

        #region Field
        private ObservableCollection<Picture> _Results;
        private Filter _currentFiltter;
        private bool _showFilters;
        private List<Filter> _filters;
        private string _searchText;
        private IPictureRepositoryFactory _pictureRepositoryFactory;
        #endregion

        #region Constrcutor
        public SearchResultsPageViewModel()
            : this(new PictureRepositoryFactory())
        {

        }

        public SearchResultsPageViewModel(IPictureRepositoryFactory pictureRepositoryFactory)
        {
            _pictureRepositoryFactory = pictureRepositoryFactory;
        }

        #endregion

        #region Property

        public ObservableCollection<Picture> Results
        {
            get
            {

                if (_Results == null)
                    _Results = new ObservableCollection<Picture>();

                _Results.Clear();


                if (this.AllSearchResultPictures == null)
                    return null;

                List<Picture> filtterResult = this.AllSearchResultPictures;
                if (this.CurrentFiltter != null && this.CurrentFiltter.Name != "All")
                {
                    filtterResult = this.AllSearchResultPictures.Where(x => x.ParentContianer.Title == this.CurrentFiltter.Name).ToList();
                }

                foreach (var item in filtterResult)
                {
                    _Results.Add(item);
                }

                return _Results;

            }
        }

        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                SetProperty(ref _searchText, value);
                OnPropertyChanged("QueryText");
            }
        }

        public string QueryText { get { return string.Format("\"{0}\"", SearchText); } }

        public bool ShowFilters
        {
            get
            {
                return _showFilters;
            }
            set
            {
                SetProperty(ref _showFilters, value);
            }
        }

        public List<Filter> Filters
        {
            get
            {
                return _filters;
            }
            set
            {
                SetProperty(ref _filters, value);
            }
        }

        private Filter CurrentFiltter
        {
            get
            {
                return _currentFiltter;
            }
            set
            {
                SetProperty(ref _currentFiltter, value);
            }
        }

        public List<Picture> AllSearchResultPictures { get; set; }
        #endregion

        #region Command
        ICommand _FiltterChangeCommand;
        public ICommand FiltterChangeCommand
        {
            get
            {
                if (_FiltterChangeCommand == null)
                    _FiltterChangeCommand = new RelayCommand<Filter>(OnFiltterChange);

                return _FiltterChangeCommand;
            }
        }

        private void OnFiltterChange(Filter filter)
        {
            CurrentFiltter = filter;
            OnPropertyChanged("Results");

        }

        ICommand _PictureSelectedCommand;
        public ICommand PictureSelectedCommand
        {
            get
            {
                if (_PictureSelectedCommand == null)
                    _PictureSelectedCommand = new RelayCommand<Picture>(OnPictureSelected);

                return _PictureSelectedCommand;
            }
        }

        private void OnPictureSelected(Picture filter)
        {
            if (filter == null)
                return;

            Gallery newGallery = filter.ParentContianer;
            var pictureList = new List<Picture>();
            pictureList.AddRange(filter.ParentContianer.Pictures);
            newGallery.index = pictureList.IndexOf(filter, 0);

            this.NavigationService.Navigate("View.ImagePlayer", newGallery.Id.ToString());
        }


        ICommand _GoBackCommand;
        public ICommand GoBackCommand
        {
            get
            {
                if (_GoBackCommand == null)
                    _GoBackCommand = new RelayCommand(OnGoBack);

                return _GoBackCommand;
            }
        }

        private void OnGoBack()
        {
            if (this.NavigationService.CanGoback)
                this.NavigationService.GoBack();
            else
                this.NavigationService.Navigate("View.Home", null);
        }

        #endregion

        #region Method
        public override void LoadState(object navigationParameter, Dictionary<string, object> vmState)
        {
            var queryText = navigationParameter as String;

            SearchText = queryText;
            DoSearch();
        }

        private async void DoSearch()
        {
            IPictureRepository _GelleryManager = _pictureRepositoryFactory.CreatePicturerepository();

            IEnumerable<Picture> pic = await _GelleryManager.Search(SearchText);
            this.AllSearchResultPictures = pic.ToList();
            var pictures = this.AllSearchResultPictures;

            var filterList = new List<Filter>();
            //all
            var allFilter = new Filter("All", pictures.Count(), true);
            filterList.Add(allFilter);

            int picCount = 0;
            if (pictures != null)
            {
                filterList.AddRange(pictures.Select(x => x.ParentContianer.Title).Distinct().Select(x => new Filter(x, pictures.Where(p => p.ParentContianer.Title == x).Count(), false)));
                picCount = pictures.Count();
            }

            Filters = filterList;
            ShowFilters = filterList.Count > 1;

        }
        #endregion

    }
}
