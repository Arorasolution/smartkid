﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using SmartKid.BusinessLayer;
using SmartKid.Common.Model;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using SmartKid.Utility;
using Windows.ApplicationModel.Store;
using Windows.UI.Popups;
using System.Threading.Tasks;

namespace SmartKid.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {

        private readonly ILicenseManager _licenseManager;
        IPictureRepositoryFactory _pictureRepositoryFactory;

        #region Constrcutor
        public HomeViewModel()
            : this(new PictureRepositoryFactory(), new LicenseManager())
        {

        }

        public HomeViewModel(IPictureRepositoryFactory pictureRepositoryFactory, ILicenseManager _LicenseManager)
        {
            _licenseManager = _LicenseManager;
            _pictureRepositoryFactory = pictureRepositoryFactory;
        }

        #endregion

        #region Property

        private ObservableCollection<Gallery> _gallery;
        public ObservableCollection<Gallery> Gallery
        {
            get
            {
                return _gallery;
            }
            set
            {
                SetProperty(ref _gallery, value);
            }
        }

        #endregion

        #region Command
        ICommand _GalleryChangeCommand;
        public ICommand GalleryChangeCommand
        {
            get
            {
                if (_GalleryChangeCommand == null)
                    _GalleryChangeCommand = new RelayCommand<Gallery>(OnGallerySelected);

                return _GalleryChangeCommand;
            }
        }

        private async void OnGallerySelected(Gallery gallery)
        {
            string galleryIdToString = gallery.Id.ToString();
            bool hasLicense = await _licenseManager.CheckProcuctLicenseAsync(galleryIdToString);
            if (hasLicense)
            {
                this.NavigationService.Navigate("View.ImagePlayer", galleryIdToString);
            }
            else
            {
                // Pop up ask user to purchase
                PromptProductPurchaseAsync(galleryIdToString);

            }
        }



        ICommand _AppPurchaseCommand;
        public ICommand AppPurchaseCommand
        {
            get
            {
                if (_AppPurchaseCommand == null)
                    _AppPurchaseCommand = new RelayCommand(PurchaseSmartBrain);

                return _AppPurchaseCommand;
            }
        }

        private void PurchaseSmartBrain()
        {
            CurrentApp.RequestAppPurchaseAsync(false);
        }
        #endregion

        #region Method
        public override async void LoadState(object navigationParameter, Dictionary<string, object> vmState)
        {
            IPictureRepository _GelleryManager = _pictureRepositoryFactory.CreatePicturerepository();

            IEnumerable<Category> categories = await _GelleryManager.GetAllCategories();
            IEnumerable<Gallery> gal = categories.ToList()[0].Galleries;

            Gallery = new ObservableCollection<Gallery>(gal);
        }

        #endregion

        private async Task<IUICommand> PromptProductPurchaseAsync(string productId)
        {
            var productListing = await _licenseManager.GetProductListingAsync(productId);
            string msg = string.Format("'{0}' gallery is LOCKED!. You can purchase the gallery", productListing.Name);

            var dialog = new MessageDialog(msg, "SmartBrain License");
            dialog.CancelCommandIndex = 1;

            var purchaseCmd = new UICommand(string.Format("Get {0} ({1})", productListing.Name, productListing.FormattedPrice),
                                             ShowProductPurchaseUI,
                                             productListing.ProductId);

            var closeCmd = new UICommand("Close", null, "close");
            dialog.Commands.Add(purchaseCmd);
            dialog.Commands.Add(closeCmd);

            var cmd = await dialog.ShowAsync();
            return cmd;
        }

        private async void ShowProductPurchaseUI(IUICommand cmd)
        {
            try
            {
                string featureId = cmd.Id.ToString();

                string receipt = await _licenseManager.RequestFeaturePurchaseAsync(featureId);
                // cancelled

                if (string.IsNullOrEmpty(receipt))
                    return;

                bool isActive = await _licenseManager.CheckProcuctLicenseAsync(featureId);
                if (isActive)
                {
                    var featureListing = await _licenseManager.GetProductListingAsync(featureId);

                    string msg = string.Format("Great, {0} is now enabled!", featureListing.Name);
                    await new MessageDialog(msg, "Thank you").ShowAsync();
                    var gal = Gallery.FirstOrDefault(x => x.Id.ToString().ToUpper() == featureId);
                    if (gal != null)
                    {
                        gal.IsLocked = false;
                        this.NavigationService.Navigate("View.ImagePlayer", featureId);
                    }
                }
            }
            catch
            {
                var dialog = new MessageDialog("Sorry, there was a problem activiting your license, please try again.");
                dialog.ShowAsync();
            }
        }
    }
}
