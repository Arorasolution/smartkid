﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Store;
using Windows.UI.Popups;

namespace SmartKid.ViewModels
{
    public class AboutUseControlViewModel : ViewModelBase
    {
        #region Command 
        ICommand _AppPurchaseCommand;
        public ICommand AppPurchaseCommand
        {
            get
            {
                if (_AppPurchaseCommand == null)
                    _AppPurchaseCommand = new RelayCommand(PurchaseSmartBrain);

                return _AppPurchaseCommand;
            }
        }

        private void PurchaseSmartBrain()
        {
            CurrentApp.RequestAppPurchaseAsync(false);
        }
        #endregion
    }
}
