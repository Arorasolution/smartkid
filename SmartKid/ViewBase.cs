﻿using SmartKid.Common;
using SmartKid.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace SmartKid
{
    public abstract class ViewBase : LayoutAwarePage
    {
        protected override void LoadState(object navigationParameter, Dictionary<string, object> pageState)
        {
            var vm = DataContext as ViewModelBase;
            if (vm != null)
            {
                vm.LoadState(navigationParameter, pageState);
            }
        }

        protected override void SaveState(Dictionary<string, object> pageState)
        {
            var vm = DataContext as ViewModelBase;
            if (vm != null)
            {
                vm.SaveState(pageState);
            }
        }

        public static readonly DependencyProperty DataContextChangeWatcherProperty =
         DependencyProperty.Register("DataContextChangeWatcher", typeof(object),
         typeof(ViewBase), new PropertyMetadata(null, OnDataContextChanged));


        private static void OnDataContextChanged(DependencyObject source,
        DependencyPropertyChangedEventArgs e)
        {
            var vm = ((ViewBase)source).DataContext as ViewModelBase;
            if (vm != null)
            {
                vm.NavigationService = NavigationService.Current;
            }
        }

        public ViewBase()
        {
            BindingOperations.SetBinding(this, DataContextChangeWatcherProperty, new Binding());
        }
    }
}
