﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartKid
{
    public interface INavigationService
    {
        void Navigate(string view, object param);
        void Navigate(string view);
        bool CanGoback { get; }
        bool CanGoForward { get; }
        void GoHome();
        void GoForward();
        void GoBack();
    }
}
