﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace SmartKid.Behaviors
{
    public static class SelecttionChangeCommandBehavior
    {
        public static ICommand GetCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("Command", typeof(ICommand), typeof(SelecttionChangeCommandBehavior), new PropertyMetadata(null, OnCommandChanged));

        public static object GetCommandParameter(DependencyObject obj)
        {
            return (object)obj.GetValue(CommandParameterProperty);
        }

        public static void SetCommandParameter(DependencyObject obj, object value)
        {
            obj.SetValue(CommandParameterProperty, value);
        }

        // Using a DependencyProperty as the backing store for CommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.RegisterAttached("CommandParameter", typeof(object), typeof(SelecttionChangeCommandBehavior), new PropertyMetadata(null));


        private static void OnCommandChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            ListViewBase s = source as ListViewBase;
            if (s == null) return;


            //s.SelectionChanged += OnSelecttion;
            s.ItemClick += s_ItemClick;
        }

        static void s_ItemClick(object sender, ItemClickEventArgs e)
        {
            ListViewBase s = sender as ListViewBase;
            ICommand cmd = s.GetValue(SelecttionChangeCommandBehavior.CommandProperty) as ICommand;
            //object parm = s.GetValue(SelecttionChangeCommandBehavior.CommandParameterProperty) as object;

            if (cmd != null && cmd.CanExecute(e.ClickedItem))
            {
                cmd.Execute(e.ClickedItem);
            }
        }

        private static void OnSelecttion(object sender, SelectionChangedEventArgs e)
        {
            ListViewBase s = sender as ListViewBase;
            ICommand cmd = s.GetValue(SelecttionChangeCommandBehavior.CommandProperty) as ICommand;
            object parm = s.GetValue(SelecttionChangeCommandBehavior.CommandParameterProperty) as object;

            if (cmd != null && cmd.CanExecute(parm))
            {
                cmd.Execute(parm);
            }
        }

    }
}
