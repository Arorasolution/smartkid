﻿using SmartKid.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartKid.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ImagePlayer : ViewBase
    {
        public ImagePlayer()
        {
            this.InitializeComponent();
        }
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            ((ImagePlayerViewModel)DataContext).Player = this.mediaControl;
            ((ImagePlayerViewModel)DataContext).Appbar = MainAppbar;
            base.LoadState(navigationParameter, pageState);
        }

    }
}
